import './App.css';
import PostsList from './features/postsSlice/PostsList';
import AddPostsForm from './features/postsSlice/AddPostsForm';

function App() {
  return(
    <div className='App'>
      <PostsList/>
      <AddPostsForm/>
    </div>
  )
}

export default App;
