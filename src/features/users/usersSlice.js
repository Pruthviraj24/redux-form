import {createSlice} from "@reduxjs/toolkit"

const initialState = []

const usersSlice = createSlice({
    name:'users',
    initialState,
    reducers:{
            usersAdded:{
                reducer(state,action){
                state.push(action.payload)
        }
    }
}
})

export const selectAllUsers = (state) => state.users

export default usersSlice.reducer