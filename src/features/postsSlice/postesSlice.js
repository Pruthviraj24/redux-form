import {createSlice, nanoid} from "@reduxjs/toolkit"



const initialState = []

const postsSlice = createSlice({
    name:'posts',
    initialState,
    reducers:{
        postAdded:{
            reducer(state,action){
            state.push(action.payload)
        },
        prepare(title,content,userName){
            return{
                payload:{
                    id:nanoid(),
                    title,
                    content,
                    userName,
                    date:new Date().toISOString(),
                    reactions:{
                        like:0,
                        dislike:0,
                        love:0
                    }
                }
            }
        }
    },
    deletePost:{
        reducer(state,action){
        return state.filter(eachPost => eachPost.id !== action.payload)
    }
    },
    addReactions(state,action){
            const {postId,reaction} = action.payload
            console.log(reaction);
            const existingPost =  state.find(post => post.id === postId)
            if(existingPost){
                existingPost.reactions[reaction]++
            }
        }
}})

export const selectAllPosts = (state)=> state.posts;

export const {postAdded,deletePost,addReactions} = postsSlice.actions


export default postsSlice.reducer


