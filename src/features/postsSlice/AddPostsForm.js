import {useState} from "react"
import {useDispatch} from "react-redux"

import {postAdded} from "./postesSlice"
import "../../appStyles/index.css"

function AddPostsForm() {
    const dispatch = useDispatch()

    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const [userName,setUsers] = useState('')

    const onTitleChanged = e => setTitle(e.target.value)
    const onContentChaneged = e => setContent(e.target.value)
    const onAuthoreChaneged = e => setUsers(e.target.value)
    

    const onSavepostClicked = ()=>{
        
        if(title && content){
            dispatch(
                postAdded(title,content,userName)
            )
        setTitle('')
        setContent('')
        setUsers('')
        }
    }


    const canSave = title && content

  return (
    <div className="inputs-container">
   
        <form className="form-container">
        <h2>Add a new post</h2>
            <label htmlFor="postTitle">Post Title:</label>
            <input
                type="text"
                id="postTitle"
                name="postTitle"
                value={title}
                placeholder="Enter Title"
                onChange={onTitleChanged}
            />

            <label htmlFor="userIds">Select User</label>
            <input placeholder="Eneter Username" type="text" id="userIds" value={userName} onChange={onAuthoreChaneged}/>
            <label htmlFor="postContent">Content:</label>
            <textarea
            id="postContent"
            name="postContent"
            value={content}
            onChange={onContentChaneged}
            placeholder="Enter blog content"
            />
            <button className="save-post-button" onClick={onSavepostClicked} type="button" disabled={!canSave}>Save post</button>
        </form>        
    </div>
  )
}

export default AddPostsForm