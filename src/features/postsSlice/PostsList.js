import { useSelector,useDispatch } from 'react-redux'
import { selectAllPosts } from './postesSlice'
import {deletePost,addReactions} from "./postesSlice"
import React from 'react'
import {AiFillLike} from "react-icons/ai"
import TimeAgo from "./TimeAgo"
import PostAuthor from './PostAuthor'
import "../../appStyles/index.css"


const PostsList = () => {
    const posts = useSelector(selectAllPosts)
    const dispatch = useDispatch()


    const orderdPost = posts.slice().sort((a,b)=>b.date.localeCompare(a.date))

    const deleteBlogPost = (e)=>{
      console.log(e.target);
      dispatch(
        deletePost(e.target.value)
      )
    }


   
    const renderPosts = orderdPost.map(post=>{

      const updateReaction = (e)=>{
            dispatch(
              addReactions({postId:post.id,reaction:e.target.value})
           )
        }


      return(
        <article className='bg-main-container'  key={post.id}>
            <div>
              <h3>Post Title:  <br/><br/> {post.title}</h3>
              <p>Content:  <br/><br/>  {post.content}</p>
              <p>Author: <PostAuthor userName = {post.userName}/></p>
              <p><TimeAgo timestamp = {post.date}/></p>
              <button className='delete-post' value={post.id} onClick={deleteBlogPost}>Delete Post</button>
            </div>
            <div className='reactions-contianer'>
              <div>
                <button  className='reaction-button' value="like" onClick={updateReaction}>
                    Like
                </button>
                <p  className='reaction-count'>{post.reactions.like}</p>
              </div>
              <div>
                <button className='reaction-button' value="dislike" onClick={updateReaction}  type="button">Dislike</button>
                <p  className='reaction-count'>{post.reactions.dislike}</p>
              </div>
              <div>
                <button className='reaction-button' value="love" onClick={updateReaction}  type="button">Love</button>
                <p className='reaction-count'>{post.reactions.love}</p>
              </div>
            </div>
        </article>
      )
    })


  return (
    <div className='blogs-container'>
        <h2 className='posts-header'>Blog Posts</h2>
        {renderPosts}
    </div>
  )
}


export default PostsList